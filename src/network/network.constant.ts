export enum NetworkStatus {
    None = 'None',
    Started = 'Started',
    Done = 'Done',
    Failed = 'Failed',
}
