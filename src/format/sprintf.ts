export function sprintf(stringValue: string, ...params: (number | string)[]): string {
    let str = stringValue;
    const len = params.length;
    const emptyIndices: number[] = [];

    const stringParams: string[] = params.map(item => {
        return String(item);
    });

    // replace placeholders with existing arguments
    str = str.replace(/\{(\d+)}/g, (replacedStr: string, index: number) => {
        const empty = index >= len || params[index] === null;
        if (empty && emptyIndices.indexOf(index) === -1) {
            emptyIndices.push(index);
        }

        return empty
            ? replacedStr
            : stringParams[index];
    });

    // rename non-replaced placeholders, e.g. "{3} {15} {5}" -> "{0} {2} {1}"
    if (emptyIndices.length) {
        emptyIndices.sort((a, b) => {
            return a - b;
        });

        emptyIndices.forEach((index, i) => {
            str = str.replace(new RegExp('\\{' + index + '\\}', 'g'), () => {
                return '{' + i + '}';
            });
        });
    }

    return str;
}
