import {sprintf} from '../sprintf';

describe('sprintf', () => {
    test('simple format string should work correctly', () => {
        expect(sprintf('simple string'))
            .toBe('simple string');
    });

    test('string format should work correctly with params', () => {
        expect(sprintf('parametrize string {0}, {1}', 'param1', 'param2'))
            .toBe('parametrize string param1, param2');
    });

    test('string format should work correctly with empty params', () => {
        expect(sprintf('parametrize string {0}, {1}', 'param1'))
            .toBe('parametrize string param1, {0}');
    });
});
