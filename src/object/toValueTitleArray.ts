export function toValueTitleArray<T>(obj: Object): any[] {
    return Object
        .keys(obj)
        .map(key => ({
            value: key,
            title: obj[key],
        }));
}
