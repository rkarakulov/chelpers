export function withEndSlash(url: string): string {
    const slash = url.endsWith('/')
        ? ''
        : '/';
    return url + slash;
}
