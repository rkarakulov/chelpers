import {getUrlParams} from '../getUrlParams';

describe('getUrlParams', () => {
    test('works with different types of urls', () => {
        expect(getUrlParams('http://spotware.com?jasmine=ok'))
            .toEqual({jasmine: 'ok'});
        expect(getUrlParams('https://spotware.com?jasmine=ok'))
            .toEqual({jasmine: 'ok'});
        expect(getUrlParams('https://spotware.com?jasmine=ok#hash'))
            .toEqual({jasmine: 'ok'});
        expect(getUrlParams('https://spotware.com?jasmine=ok#hash=hashvalue&foo=bar'))
            .toEqual({jasmine: 'ok'});
    });

    test('works with different get params', () => {
        expect(getUrlParams('http://spotware.com?jasmine=ok&foo'))
            .toEqual({
                jasmine: 'ok',
                foo: undefined,
            });
        expect(getUrlParams('http://spotware.com?jasmine=ok&foo&bar=true'))
            .toEqual({
                jasmine: 'ok',
                foo: undefined,
                bar: 'true',
            });
    });
});
