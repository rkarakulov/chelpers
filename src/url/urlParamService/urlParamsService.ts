import {IUrlParamService} from './urlParamsService.interface';
import {IUrlParams} from './urlParams.interface';
import {getUrlParams} from './getUrlParams';
import {param} from './param';
import {parseUrl} from '../parseUrl/parseUrl';
import {constructUrl} from '../constructUrl';

export class UrlParamService implements IUrlParamService {
    private initialQueryParams: IUrlParams;

    constructor() {
        this.initialQueryParams = getUrlParams(location.href);
        this.removeParameterFromUrl('demo');
        this.removeParameterFromUrl('lang');
        this.removeParameterFromUrl('vtoken');
    }

    public getInitialParams(): IUrlParams {
        return this.initialQueryParams;
    }

    public has(paramName: keyof IUrlParams): boolean {
        return paramName in this.initialQueryParams;
    }

    public get(paramName: keyof IUrlParams): any {
        return this.initialQueryParams[paramName];
    }

    public length(): number {
        return Object.keys(this.initialQueryParams).length;
    }

    public removeParameterFromUrl(paramName: string, url?: string): void {
        const urlStruct = parseUrl(url);
        if (urlStruct.search) {
            const params = getUrlParams(url);
            delete params[paramName];
            urlStruct.search = '?' + param(params);
        }
        const newUrl = constructUrl(urlStruct);
        if (url !== newUrl) {
            try {
                window.history.replaceState({}, '', newUrl);
            } catch (e) {
                location.href = newUrl;
            }
        }
    }

    public pop(paramName: keyof IUrlParams): any {
        const value = this.initialQueryParams[paramName];
        this.removeParameterFromUrl(paramName);
        return value;
    }
}
