import {IUrlParams} from './urlParams.interface';
import {parseUrl} from '../parseUrl/parseUrl';

export function getUrlParams(url: string): IUrlParams {
    const params = {};
    const queryString = parseUrl(url)
        .search
        .substr(1); // remove "?" from the start

    if (queryString) {
        queryString
            .split('&')
            .forEach((data: string) => {
                const param = data.split('=');
                params[param[0]] = param[1] && decodeURIComponent(param[1].replace(/\+/g, '%20'));
            });
    }

    return params;
}
