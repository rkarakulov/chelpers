export function param(arr: { [key: string]: any }) {
    return Object
        .keys(arr)
        .map(key => {
            if (Array.isArray(arr[key])) {
                const arrayParts = [];
                for (let i = 0; i < arr[key].length; i++) {
                    arrayParts.push(encodeURIComponent(key + '[]') + '=' + encodeURIComponent(arr[key][i]));
                }
                return arrayParts.join('&');
            }
            return encodeURIComponent(key) + '=' + encodeURIComponent(arr[key]);
        })
        .join('&');
}
