import {IUrlParams} from './urlParams.interface';

export interface IUrlParamService {
    getInitialParams(): IUrlParams;

    has(paramName: keyof IUrlParams): boolean;

    get(paramName: keyof IUrlParams): any;

    length(): number;

    removeParameterFromUrl(paramName: string, url?: string): void;

    pop(paramName: keyof IUrlParams): any;
}
