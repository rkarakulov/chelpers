export interface IUrlParams {
    redirectTo?: string;
    action?: string;

    cServerProxyListUrl?: string;
    cServerProxyHost?: string;
    cServerProxyPort?: number;
    cHubProxyListUrl?: string;
    cHubProxy?: string;

    // force old json websocket connection (not binary, without protobuf)
    oldWebsocket?: undefined;

    // force language
    lang?: string;

    // force trading account to login to on start
    acc?: string;

    // uses specific authServiceUrl service url (usually you want to change it together with hub url).
    // http://kawase.dev.trader.dp.local/?authServiceUrl=https://cons-id-webdev.ctrader.com:1443/
    authServiceUrl?: string;

    // do not show redirectToAppIfRequired dialog on mobile
    // (skip setting from configuration, which forces user to use mobile app rather than mobile version of trader)
    ignoreForceUsingMobileApp?: undefined;

    // override GUI settings CTID.ForceLoginToCTIDOnStartup value
    forceCTID?: boolean | string;

    // force using gui_settings_mock.ts instead of gui settings from cHub
    mockGuiSettings?: undefined;

    // force to show depth lists in symbol tree
    showDoM?: undefined;

    // log symbols spot prices and VWAP calculations
    logSymbolIds?: string | string[];

    plantId?: string;

    // force country settings (see ExtraIdParams)
    countryName?: string;
    countryIsoCode?: string;

    // see DinPay and PaySafe
    customerIp?: string;

    // token from MyLCG
    vtoken?: string;

    // limit rows for IProtoDealListReq
    dealsMaxRows?: number;

    // force some symbols to be absent on server (for watchlists debug)
    // example: ?absentSymbols=EURUSD,AUDJPY,GBPUSD
    absentSymbols?: string;

    // force LCG demo mode
    demo?: undefined;

    // collect missing translations to local storage:
    // localStorage.getItem('missingTranslations')
    collectMissingTranslations?: undefined;

    hLog?: number;
    sLog?: number;
    // mirror log
    cLog?: number;
    // deals connection log
    dLog?: number;

    // CM-1923 cMirror for FxPro
    key?: string;
}
