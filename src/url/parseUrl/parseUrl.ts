import {IParsedUrl} from './parseUrl.interface';
import {getUrl} from '../getUrl';

export const parseSearchParams = (searchString: string) => {
    const queryString = searchString.substr(1);
    const params = {};
    if (queryString) {
        queryString
            .split('&')
            .forEach(data => {
                const param = data.split('=');
                params[param[0]] = param[1] && decodeURIComponent(param[1].replace(/\+/g, '%20'));
            });
    }
    return params;
};

export const parseUrl = (url: string = getUrl()): IParsedUrl => {
    const a = document.createElement('a');
    a.href = url;
    return {
        href: a.href,
        protocol: a.protocol,
        host: a.host.split(':')[0], // fix for ie11
        port: Number(a.port) || undefined,
        pathname: a.pathname || '',
        search: decodeURIComponent(a.search || ''),
        searchParams: parseSearchParams(decodeURIComponent(a.search || '')),
        hash: decodeURIComponent(a.hash || ''),
    };
};
