import {IUrlParams} from '../urlParamService/urlParams.interface';

export interface IParsedUrl {
    href: string;
    protocol: string;
    host: string;
    port: number;
    pathname: string;
    search: string;
    hash: string;
    searchParams: IUrlParams;
}
