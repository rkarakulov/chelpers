import {parseSearchParams, parseUrl} from '../parseUrl';

describe('parseUrl', () => {
    test('handles android urls', () => {
        const url = 'file:///android_asset/www/index.html?demo&user=test';
        const expectedParsedResult = {
            href: 'file:///android_asset/www/index.html?demo&user=test',
            protocol: 'file:',
            host: '',
            port: undefined,
            pathname: '/android_asset/www/index.html',
            search: '?demo&user=test',
            hash: '',
            searchParams: {
                demo: undefined,
                user: 'test',
            },
        };
        const actualParseResult = parseUrl(url);
        expect(actualParseResult)
            .toEqual(expectedParsedResult);
    });

    test('parses websocket url', () => {
        const url = 'wss://h4.p.ctrader.cn:80/websocket';
        const expectedParsedResult = {
            href: 'wss://h4.p.ctrader.cn:80/websocket',
            protocol: 'wss:',
            host: 'h4.p.ctrader.cn',
            port: 80,
            pathname: '/websocket',
            search: '',
            hash: '',
            searchParams: {},
        };
        const actualParseResult = parseUrl(url);
        expect(actualParseResult)
            .toEqual(expectedParsedResult);
    });

    test('parses url with standard ports', () => {
        const url = 'wss://h4.p.ctrader.cn/websocket';
        const expectedParsedResult = {
            href: 'wss://h4.p.ctrader.cn/websocket',
            protocol: 'wss:',
            port: undefined,
            host: 'h4.p.ctrader.cn',
            pathname: '/websocket',
            search: '',
            hash: '',
            searchParams: {},
        };
        const actualParseResult = parseUrl(url);
        expect(actualParseResult)
            .toEqual(expectedParsedResult);
    });
});

describe('parseSearchParams', () => {
    test('should parse query string to object', () => {
        const expected = {
            prop1: 'prop1value',
            prop2: 'prop2value',
            prop3: 'prop3value',
        };
        const query = Object.keys(expected)
                            .reduce((res, currentKey) => res + `${currentKey}=${expected[currentKey]}&`, '?');
        const actual = parseSearchParams(query);
        expect(actual)
            .toEqual(expected);
    });
});
