import {IParsedUrl} from './parseUrl/parseUrl.interface';

const secureProtocols = ['wss:', 'https:'];
const insecureProtocols = ['ws:', 'http:'];
const securePort = 443;
const insecurePort = 80;

export function constructUrl(obj: IParsedUrl = <any> location): string {
    if (obj.protocol && obj.protocol.indexOf(':') === -1) {
        throw new Error('Error in PROTOCOL - should contain ":"');
    }
    if (obj.host && obj.host.indexOf(':') !== -1) {
        throw new Error('Error in HOST - should not contains :PORT');
    }
    if (!obj.host && obj.port) {
        throw new Error('Did not expect port without host');
    }

    let url = '';
    url += obj.protocol;
    url += '//';
    url += obj.host;

    if (obj.port) {
        if (typeof obj.port !== 'number') {
            throw new Error('If port specified, it should be a number');
        }
        const shouldPortBeOmited = obj.port === securePort && secureProtocols.indexOf(obj.protocol) !== -1
            || obj.port === insecurePort && insecureProtocols.indexOf(obj.protocol) !== -1;
        if (!shouldPortBeOmited) {
            url += ':';
            url += obj.port;
        }
    }

    let pathname = obj.pathname || '/';

    if (pathname[0] !== '/') {
        pathname = '/' + pathname;
    }

    url += pathname;

    if (obj.search) {
        if (obj.search[0] !== '?') {
            obj.search = '?' + obj.search;
        }
        url += obj.search;
    }

    if (obj.hash) {
        if (obj.hash[0] !== '#') {
            obj.hash = '#' + obj.hash;
        }
        url += obj.hash;
    }

    return url;
}
