import {constructUrl} from '../constructUrl';

describe('constructUrl', () => {
    test('correct parameters', () => {
        expect(constructUrl({
            protocol: 'http:',
            host: 'spotware.com',
            port: 8080,
            pathname: '/main',
            search: 'searching=true',
            hash: 'quickly',
        } as any))
            .toBe('http://spotware.com:8080/main?searching=true#quickly');

        expect(constructUrl({
            protocol: '',
            host: 'spotware.com',
            port: 8080,
            pathname: '/main',
            search: 'searching=true',
            hash: 'quickly',
        }as any))
            .toBe('//spotware.com:8080/main?searching=true#quickly');

        expect(constructUrl({
            protocol: 'https:',
            host: 'spotware.io',
            port: 555,
            hash: '#slowest',
        } as any))
            .toBe('https://spotware.io:555/#slowest');

        expect(constructUrl({
            protocol: 'wss:',
            host: 'spotware.net',
            port: 443,
        } as any))
            .toBe('wss://spotware.net/');

        expect(constructUrl({
            protocol: 'ws:',
            host: 'spotware.net',
            port: 443,
        } as any))
            .toBe('ws://spotware.net:443/');

        expect(constructUrl({
            protocol: 'file:',
            host: '',
            port: '',
            pathname: '/android_asset/www/index.html',
        } as any))
            .toBe('file:///android_asset/www/index.html');
    });

    test('incorrect parameters', () => {
        expect(() => {
            constructUrl({
                protocol: 'wss',
                port: 443,
            } as any);
        })
            .toThrow();

        expect(() => {
            constructUrl({
                protocol: 'http:',
                host: 'spotware.com:8080',
                port: 8080,
            } as any);
        })
            .toThrow();

        expect(() => {
            constructUrl({
                protocol: 'file:',
                host: '',
                port: 80,
                pathname: '/android_asset/www/index.html',
            } as any);
        })
            .toThrow();
    });
});
