import {withEndSlash} from '../withEndSlash';

describe('withEndSlash', () => {
    test('should return url with slash if it is not ended with /', () => {
        expect(withEndSlash('testUrl/')).toBe('testUrl/');
    });

    test('should return url with slash if is is ended with /', () => {
        expect(withEndSlash('testUrl')).toBe('testUrl/');
    });
});
