import {Container} from 'inversify';
import {memoize} from 'lodash';

const container = new Container();
const methods = 'load unload bind rebind unbind unbindAll snapshot restore applyCustomMetadataReader applyMiddleware'
    .split(' ');
methods.forEach(method => {
    const containerAny = container as any;
    const originalMethod = containerAny[method];
    containerAny[method] = function (this: Container) {
        (containerGet as any).cache.clear();
        return originalMethod.apply(this, arguments); //tslint:disable-line:no-invalid-this
    };
});

const containerGet: <T>(s: Symbol) => T = memoize(s => container.get<any>(s));

export {container};
export {containerGet};
