export {getReduxService} from './getReduxService';
export {IReduxService} from './reduxService.interface';
export {ReduxService} from './reduxService';
export {container, containerGet} from './diContainer';
export {diTypeReduxService} from './reduxService.constant';
