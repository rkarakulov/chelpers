import {ReduxService} from './reduxService';
import {diTypeReduxService} from './reduxService.constant';
import {containerGet} from './diContainer';

type IGetReduxService = <T extends object>() => ReduxService<T>;

export const getReduxService: IGetReduxService = <T extends object>() => containerGet<ReduxService<T>>(diTypeReduxService as Symbol);
