import {Action, Store} from 'redux';
import {ParametricSelector, Selector} from 'reselect';
import {Observable} from 'rxjs';

export interface IReduxService<S> {
    setStore(store: S): void;

    dispatch(action: Action): Action;

    getSelectedState<R>(selector: Selector<S, R>): R;

    getSelectedState<P, R>(selector: ParametricSelector<S, P, R>, params: P): R;

    getObservableForSelector<R>(selector: Selector<S, R>): Observable<R>;

    getObservableForSelector<P, R>(selector: ParametricSelector<S, P, R>, params: P): Observable<R>;

    getStore(): Store<S>;
}
