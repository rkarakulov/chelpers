import {ParametricSelector, Selector} from 'reselect';
import {Observable, ReplaySubject} from 'rxjs/Rx';
import {getObservableForSelector} from './getObservableForSelector';

export class StoreObservable<S> {
    private storeSubject = new ReplaySubject<S>(1);

    private store$ = this.storeSubject.asObservable();

    public getObservableForSelector<R>(selector: Selector<S, R>): Observable<R>;
    public getObservableForSelector<P, R>(selector: ParametricSelector<S, P, R>, props: P): Observable<R>;
    public getObservableForSelector(selector: any, props?: any) {
        return getObservableForSelector(selector, this.store$, props);
    }
}
