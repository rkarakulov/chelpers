import {injectable} from 'inversify';
import {Action, Store} from 'redux';
import {ParametricSelector, Selector} from 'reselect';
import {Observable} from 'rxjs/Observable';
import {IReduxService} from './reduxService.interface';
import {getSelectedState} from '../select/getSelectedState';
import {StoreObservable} from './storeObservable';

@injectable()
export abstract class ReduxService<S extends object> implements IReduxService<S> {
    protected store: Store<S>;

    protected storeObservable: StoreObservable<S> = new StoreObservable();

    public setStore(store: any) {
        this.store = store;
    }

    public dispatch(action: Action): Action {
        return this.store.dispatch(action);
    }

    public getSelectedState<R>(selector: Selector<S, R>): R;
    public getSelectedState<P, R>(selector: ParametricSelector<S, P, R>, props: P): R;
    public getSelectedState(selector: any, props?: any) {
        return getSelectedState(this.store, selector, props);
    }

    public getObservableForSelector<R>(selector: Selector<S, R>): Observable<R>;
    public getObservableForSelector<P, R>(selector: ParametricSelector<S, P, R>, props: P): Observable<R>;
    public getObservableForSelector(selector: any, props?: any) {
        return this.storeObservable.getObservableForSelector(selector, props);
    }

    /*
     * Generally you have to use one of methods above: dispatch, getObservableForSelector or getSelectedState
     */
    public getStore(): Store<S> {
        return this.store;
    }
}
