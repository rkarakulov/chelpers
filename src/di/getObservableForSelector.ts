import {isEqual} from 'lodash';
import {ParametricSelector, Selector} from 'reselect';
import {Observable} from 'rxjs';

export function getObservableForSelector<S, R>(selector: Selector<S, R>, store: Observable<S>): Observable<R>;
export function getObservableForSelector<S, P, R>(selector: ParametricSelector<S, P, R>, store: Observable<S>, props: P): Observable<R>; // tslint:disable-line:max-line-length
export function getObservableForSelector(selector: any, store: any, props?: any) {
    return store.map((state: any) => selector(state, props))
        .distinctUntilChanged((prevVal: any, nextVal: any) => isEqual(prevVal, nextVal));
}
