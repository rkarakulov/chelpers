import {createPathSelector} from '../createPathSelector';

describe('createPathSelector', () => {
    test('should create new selector by given path', () => {
        const accountId = 123;

        type State = {
            account: {
                id: number;
            };
        };
        const stateSnapshot: State = {
            account: {
                id: accountId
            }
        };

        const selector = (state: State) => state;
        const actual = createPathSelector(selector, 'account', 'id');

        expect(actual).toBeInstanceOf(Function);
        expect(actual(stateSnapshot)).toBe(accountId);
    });

    test('should work with parametric selector', () => {
        const accountId1 = 123;
        const accountId2 = 321;

        type State = {
            accounts: {
                [id: number]: { id: number };
            };
        };

        const stateSnapshot: State = {
            accounts: {
                [accountId1]: {
                    id: accountId1
                },
                [accountId2]: {
                    id: accountId2
                }
            }
        };

        const selector = (state: State, props: { id: number }) => state.accounts[props.id];
        const actual = createPathSelector(selector, 'id');

        expect(actual(stateSnapshot, {id: accountId1})).toBe(accountId1);
        expect(actual(stateSnapshot, {id: accountId2})).toBe(accountId2);
    });

    test('should be null safe', () => {
        type State = {
            account: {
                id: number;
            };
        };
        const stateSnapshot = {} as State;

        const selector = (state: State) => state;
        const actual = createPathSelector(selector, 'account', 'id');

        expect(actual(stateSnapshot)).toBe(undefined);
    });
});
