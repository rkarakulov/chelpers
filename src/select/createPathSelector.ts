import {Selector, ParametricSelector} from 'reselect';

/* one key */
export function createPathSelector<S extends object,
    R extends object,
    K1 extends keyof R>
(selector: Selector<S, R>, key1: K1): Selector<S, R[K1]>;
export function createPathSelector<S extends object,
    P extends object,
    R extends object,
    K1 extends keyof R>
(selector: ParametricSelector<S, P, R>, key1: K1): ParametricSelector<S, P, R[K1]>;

/* two keys */
export function createPathSelector<S extends object,
    R extends object,
    K1 extends keyof R,
    K2 extends keyof R[K1]>
(selector: Selector<S, R>, key1: K1, key2: K2): Selector<S, R[K1][K2]>;
export function createPathSelector<S extends object,
    P extends object,
    R extends object,
    K1 extends keyof R,
    K2 extends keyof R[K1]>
(selector: ParametricSelector<S, P, R>, key1: K1, key2: K2): ParametricSelector<S, P, R[K1][K2]>;

/* three keys */
export function createPathSelector<S extends object,
    R extends object,
    K1 extends keyof R,
    K2 extends keyof R[K1],
    K3 extends keyof R[K1][K2]>
(selector: Selector<S, R>, key1: K1, key2: K2, key3: K3): Selector<S, R[K1][K2][K3]>;
export function createPathSelector<S extends object,
    P extends object,
    R extends object,
    K1 extends keyof R,
    K2 extends keyof R[K1],
    K3 extends keyof R[K1][K2]>
(selector: ParametricSelector<S, P, R>, key1: K1, key2: K2, key3: K3): ParametricSelector<S, P, R[K1][K2][K3]>;

/* four keys */
export function createPathSelector<S extends object,
    R extends object,
    K1 extends keyof R,
    K2 extends keyof R[K1],
    K3 extends keyof R[K1][K2],
    K4 extends keyof R[K1][K2][K3]>
(selector: Selector<S, R>, key1: K1, key2: K2, key3: K3, key4: K4): Selector<S, R[K1][K2][K3][K4]>;
export function createPathSelector<S extends object,
    P extends object,
    R extends object,
    K1 extends keyof R,
    K2 extends keyof R[K1],
    K3 extends keyof R[K1][K2],
    K4 extends keyof R[K1][K2][K3]>
(selector: ParametricSelector<S, P, R>, key1: K1, key2: K2, key3: K3, key4: K4): ParametricSelector<S, P, R[K1][K2][K3][K4]>;

/* five keys */
export function createPathSelector<S extends object,
    R extends object,
    K1 extends keyof R,
    K2 extends keyof R[K1],
    K3 extends keyof R[K1][K2],
    K4 extends keyof R[K1][K2][K3],
    K5 extends keyof R[K1][K2][K3][K4]>
(selector: Selector<S, R>, key1: K1, key2: K2, key3: K3, key4: K4, key5: K5): Selector<S, R[K1][K2][K3][K4][K5]>;
export function createPathSelector<S extends object,
    P extends object,
    R extends object,
    K1 extends keyof R,
    K2 extends keyof R[K1],
    K3 extends keyof R[K1][K2],
    K4 extends keyof R[K1][K2][K3],
    K5 extends keyof R[K1][K2][K3][K4]>
(selector: ParametricSelector<S, P, R>, key1: K1, key2: K2, key3: K3, key4: K4, key5: K5): ParametricSelector<S, P, R[K1][K2][K3][K4][K5]>;

export function createPathSelector(selector: any, ...path: any[]) {
    return (state: any, props: any) => {
        let result = selector(state, props);
        let index = 0;

        while (result !== null && result !== undefined && index < path.length) {
            result = result[path[index++]];
        }

        return result;
    };
}
