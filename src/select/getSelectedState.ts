import {Store} from 'redux';
import {Selector, ParametricSelector} from 'reselect';

export function getSelectedState<S, R>(store: Store<S>, selector: Selector<S, R>): R;
export function getSelectedState<S, P, R>(store: Store<S>, selector: ParametricSelector<S, P, R>, params: P): R;
export function getSelectedState(store: any, selector: any, params?: any) {
    return selector(store.getState(), params);
}
